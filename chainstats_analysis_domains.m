clear all
close all

path='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
F=dir(path);
F=F(3:end);

counter=0;

for i=88%1:length(F)
    try
    counter=counter+1;
    %%%Extract Data
    folder=F(i).name;
    filename=['/negativeACchi_matchtri20N_Nd_' folder '_1_final.xml'];
    file=[path folder filename];
    system=Extract_XML(file);
    systemA=strip_beads(0,system,'A')
    systemD=strip_beads(0,system,'D')
    systemB=strip_beads(0,system,'B-C')
    systemA.bonds=ones(0,2);
    systemB.bonds=ones(0,2);
    systemD.bonds=ones(0,2);
    totaldim=[30 30 30];

    
%     get number of  beads
    s=split(convertCharsToStrings(folder),["A" "B" "C" "D"]);
    numA=str2num(s(2));
    numB=str2num(s(3));
    numD=str2num(s(5));
    
    %% B Not implemented Yet
%     [Bagglist]=calc_aggregates_beads(0,systemB,1.5,1)
%     systemB.dim=[1e20 1e20 1e20];
    
%     plot3(systemA.pos(:,1),systemA.pos(:,2),systemA.pos(:,3),'gs')
%     hold on
    close all
    %% A              aggregates (unwrap, bond chainstats)
    if numA<14
         [Aagglist]=calc_aggregates_beads(0,systemA,1.5,1);
         systemA.dim=[1e20 1e20 1e20];
         clear VOL SA SAVOLrat totalbeads2 Sphere;
         VOL=0;
         SA=0;
         SAVOLrat=0;
         totalbeads2=0;
         Sphere=0;
        for j=1:Aagglist.numaggs
            %%%Unwrap individual aggreates
            tempbeads=Aagglist.aggregates(Aagglist.aggregates(:,j)~=0,j);
            if length(tempbeads)>1
                aggpos=systemA.pos(tempbeads,:);
                clear systemTempAgg
                systemTempAgg.attype=repmat('A',size(aggpos,1),1);
                systemTempAgg.pos=aggpos;
                systemTempAgg.dim=[1e20 1e20 1e20];
                systemTempAgg.mass=ones(size(systemTempAgg.attype));
                systemTempAgg.bonds=ones(0,2);
                systemTempAgg.angles=[];
                for k=1:length(tempbeads)-1
                    systemTempAgg.bonds(end+1,:)=[tempbeads(k) tempbeads(k+1)]-1;
                end
                if size(systemTempAgg.attype,1)>10
                    [singleAgglist]=calc_aggregates_beads(0,systemTempAgg,1.5,1);  %attype,bonds,dim,mass,pos,angles

                    aggposwrap=aggpos;
                    for i=2:singleAgglist.numaggs
                        for q=1:3
                            %try moving both directions and see
                            pos2=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),:);
                            pos1=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,1)~=0,1),:);
                            [distancesO,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2);
                            offset=[0 0 0];
                            offset(q)=totaldim(q);
                            [distancesL,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2-offset);
                            [distancesR,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2+offset);
                            if min(distancesL)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),q)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),q)-totaldim(q);
                            elseif min(distancesR)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),q)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),q)+totaldim(q);
                            end
%                             if abs(singleAgglist.AggCOM(1,j)-singleAgglist.AggCOM(i,j))>totaldim(j)/2
%                                 if singleAgglist.AggCOM(1,j)>singleAgglist.AggCOM(i,j) 
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)+totaldim(j);
%                                 else
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)-totaldim(j);
%                                 end
%                             end
                        end
%                         close all

                    end
%                     close all
                    shp=alphaShape(aggposwrap(:,1),aggposwrap(:,2),aggposwrap(:,3));
                    plot(shp)
                    hold on
                    xlabel('x')
                    ylabel('y')
                    zlabel('z')

                    %change positions
                    systemA.pos(tempbeads,:)=aggposwrap;
                    
                    %record SA and vol
                    shp=alphaShape(aggposwrap(:,1),aggposwrap(:,2),aggposwrap(:,3));
                    plot(shp)
                    VOL=VOL+volume(shp)*size(aggposwrap,1);
                    SA=SA+surfaceArea(shp)*size(aggposwrap,1);
                    SAVOLrat=SAVOLrat+(surfaceArea(shp)/volume(shp))*size(aggposwrap,1);
                    Sphere=Sphere+(((pi^(1/3))*(6*volume(shp))^(2/3))/surfaceArea(shp))*size(aggposwrap,1);
                    totalbeads2=totalbeads2+size(aggposwrap,1);
                    

                    %bondtogether
                    [singleAgglist2]=calc_aggregates_beads(0,systemTempAgg,1.5,1);
                    for k=1:singleAgglist2.numaggs
                        tempbeads2=singleAgglist2.aggregates(singleAgglist2.aggregates(:,k)~=0,k);
                        for p=1:length(tempbeads2)-1
                            systemA.bonds(end+1,:)=[tempbeads(tempbeads2(p)) tempbeads(tempbeads2(p+1))]-1;
                        end
                    end
                end
            end
        end

    %     plot3(systemA.pos(:,1),systemA.pos(:,2),systemA.pos(:,3),'mo')
        %%%Chain Stats
        Achainstats=Chain_Statistics_single(0,systemA,0)
        Achainstats.Rg_eigenvalues=sort(Achainstats.Rg_eigenvalues,2,'descend')

        %Calc Asphereicity
        clear E1 E2 asphere totalbeads
        E1=0;
        E2=0;
        totalbeads=0;
    %     asphere=0;
        nbeadsperchain=Achainstats.nbeads_per_chain(Achainstats.nbeads_per_chain>1);
        for k=1:size(Achainstats.Rg,1)
            if nbeadsperchain(k)>10
                E1=sum(Achainstats.Rg_eigenvalues(k,:))^2;
                E2=Achainstats.Rg_eigenvalues(k,1)*Achainstats.Rg_eigenvalues(k,2)+Achainstats.Rg_eigenvalues(k,2)*Achainstats.Rg_eigenvalues(k,3)+Achainstats.Rg_eigenvalues(k,1)*Achainstats.Rg_eigenvalues(k,3);
                asphere(k)=(1-3*(E2/E1))*nbeadsperchain(k);
                totalbeads=totalbeads+nbeadsperchain(k);
            else
                asphere(k)=NaN;
            end

        end

        %Calc eignvalues (really just remove the ones for very small domains)
        %and aspect ratios
        clear Rg_eig AspectRat
        for k=1:length(nbeadsperchain)
            if nbeadsperchain(k)>10
                Rg_eig(k,:)=Achainstats.Rg_eigenvalues(k,:)*nbeadsperchain(k);
                AspectRat(k,:)=[Rg_eig(k,2)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,2)]*nbeadsperchain(k);
            else
                Rg_eig(k,:)=[NaN NaN NaN];
                AspectRat(k,:)=[NaN NaN NaN];
            end
        end




        Results(counter).Name=folder;
        Results(counter).A.NumDomains=sum(nbeadsperchain>10);
        Results(counter).A.asphere=nansum(asphere)/totalbeads;
        Results(counter).A.Rg_eignevalues=nansum(Rg_eig,1)/totalbeads;
        Results(counter).A.AspectRatios=nansum(AspectRat,1)/totalbeads;
        Results(counter).A.VOL=VOL/totalbeads2;
        Results(counter).A.SA=SA/totalbeads2;
        Results(counter).A.SAVOLrat=SAVOLrat/totalbeads2;
        Results(counter).A.Sphere=Sphere/totalbeads2;
    end
    Results.A
    %% D
    close all
    if numD<14
         [Dagglist]=calc_aggregates_beads(0,systemD,1.5,1);
         systemD.dim=[1e20 1e20 1e20];
         clear VOL SA SAVOLrat totalbeads2 Sphere;
         VOL=0;
         SA=0;
         SAVOLrat=0;
         totalbeads2=0;
         Sphere=0;
        for j=1:Dagglist.numaggs
            %%%Unwrap individual aggreates
            tempbeads=Dagglist.aggregates(Dagglist.aggregates(:,j)~=0,j);
            if length(tempbeads)>1
                aggpos=systemD.pos(tempbeads,:);
                clear systemTempAgg
                systemTempAgg.attype=repmat('D',size(aggpos,1),1);
                systemTempAgg.pos=aggpos;
                systemTempAgg.dim=[1e20 1e20 1e20];
                systemTempAgg.mass=ones(size(systemTempAgg.attype));
                systemTempAgg.bonds=ones(0,2);
                systemTempAgg.angles=[];
                for k=1:length(tempbeads)-1
                    systemTempAgg.bonds(end+1,:)=[tempbeads(k) tempbeads(k+1)]-1;
                end
                if size(systemTempAgg.attype,1)>10
                    [singleAgglist]=calc_aggregates_beads(0,systemTempAgg,1.5,1);  %attype,bonds,dim,mass,pos,angles

                    aggposwrap=aggpos;
                    for i=2:singleAgglist.numaggs
                        for j=1:3
                            %try moving both directions and see
                            pos2=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),:);
                            pos1=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,1)~=0,1),:);
                            [distancesO,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2);
                            offset=[0 0 0];
                            offset(j)=totaldim(j);
                            [distancesL,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2-offset);
                            [distancesR,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2+offset);
                            if min(distancesL)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)-totaldim(j);
                            elseif min(distancesR)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)+totaldim(j);
                            end
%                             if abs(singleAgglist.AggCOM(1,j)-singleAgglist.AggCOM(i,j))>totaldim(j)/2
%                                 if singleAgglist.AggCOM(1,j)>singleAgglist.AggCOM(i,j) 
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)+totaldim(j);
%                                 else
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)-totaldim(j);
%                                 end
%                             end
                        end
                    end
                    % close all
                    aggposx=aggposwrap(:,1);
                    aggposy=aggposwrap(:,2);
                    aggposz=aggposwrap(:,3);
                    plot3(aggposx,aggposy,aggposz,'bs')
                    hold on
                    xlabel('x')
                    ylabel('y')
                    zlabel('z')

                    %change positions
                    systemD.pos(tempbeads,:)=aggposwrap;
                    
                    %record SA and vol
                    shp=alphaShape(aggposwrap(:,1),aggposwrap(:,2),aggposwrap(:,3));
                    plot(shp)
                    VOL=VOL+volume(shp)*size(aggposwrap,1);
                    SA=SA+surfaceArea(shp)*size(aggposwrap,1);
                    SAVOLrat=SAVOLrat+(surfaceArea(shp)/volume(shp))*size(aggposwrap,1);
                    Sphere=Sphere+(((pi^(1/3))*(6*volume(shp))^(2/3))/surfaceArea(shp))*size(aggposwrap,1);
                    totalbeads2=totalbeads2+size(aggposwrap,1);
                    

                    %bondtogether
                    [singleAgglist2]=calc_aggregates_beads(0,systemTempAgg,1.5,1);
                    for k=1:singleAgglist2.numaggs
                        tempbeads2=singleAgglist2.aggregates(singleAgglist2.aggregates(:,k)~=0,k);
                        for p=1:length(tempbeads2)-1
                            systemD.bonds(end+1,:)=[tempbeads(tempbeads2(p)) tempbeads(tempbeads2(p+1))]-1;
                        end
                    end
                end
            end
        end

    %     plot3(systemD.pos(:,1),systemD.pos(:,2),systemD.pos(:,3),'mo')
        %%%Chain Stats
        Dchainstats=Chain_Statistics_single(0,systemD,0)
        Dchainstats.Rg_eigenvalues=sort(Dchainstats.Rg_eigenvalues,2,'descend')

        %Calc Asphereicity
        clear E1 E2 asphere totalbeads
        E1=0;
        E2=0;
        totalbeads=0;
    %     asphere=0;
        nbeadsperchain=Dchainstats.nbeads_per_chain(Dchainstats.nbeads_per_chain>1);
        for k=1:size(Dchainstats.Rg,1)
            if nbeadsperchain(k)>10
                E1=sum(Dchainstats.Rg_eigenvalues(k,:))^2;
                E2=Dchainstats.Rg_eigenvalues(k,1)*Dchainstats.Rg_eigenvalues(k,2)+Dchainstats.Rg_eigenvalues(k,2)*Dchainstats.Rg_eigenvalues(k,3)+Dchainstats.Rg_eigenvalues(k,1)*Dchainstats.Rg_eigenvalues(k,3);
                asphere(k)=(1-3*(E2/E1))*nbeadsperchain(k);
                totalbeads=totalbeads+nbeadsperchain(k);
            else
                asphere(k)=NaN;
            end

        end

        %Calc eignvalues (really just remove the ones for very small domains)
        %and aspect ratios
        clear Rg_eig AspectRat
        for k=1:length(nbeadsperchain)
            if nbeadsperchain(k)>10
                Rg_eig(k,:)=Dchainstats.Rg_eigenvalues(k,:)*nbeadsperchain(k);
                AspectRat(k,:)=[Rg_eig(k,2)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,2)]*nbeadsperchain(k);
            else
                Rg_eig(k,:)=[NaN NaN NaN];
                AspectRat(k,:)=[NaN NaN NaN];
            end
        end




        Results(counter).Name=folder;
        Results(counter).D.NumDomains=sum(nbeadsperchain>10);
        Results(counter).D.asphere=nansum(asphere)/totalbeads;
        Results(counter).D.Rg_eignevalues=nansum(Rg_eig,1)/totalbeads;
        Results(counter).D.AspectRatios=nansum(AspectRat,1)/totalbeads;
        Results(counter).D.VOL=VOL/totalbeads2;
        Results(counter).D.SA=SA/totalbeads2;
        Results(counter).D.SAVOLrat=SAVOLrat/totalbeads2;
        Results(counter).D.Sphere=Sphere/totalbeads2;
    end
    
    Results.D
    
    %% BC
    if numB<7 
         [Bagglist]=calc_aggregates_beads(0,systemB,1.5,1);
         systemB.dim=[1e20 1e20 1e20];
         clear VOL SA SAVOLrat totalbeads2 Sphere;
         VOL=0;
         SA=0;
         SAVOLrat=0;
         totalbeads2=0;
         Sphere=0;
        for j=1:Bagglist.numaggs
            %%%Unwrap individual aggreates
            tempbeads=Bagglist.aggregates(Bagglist.aggregates(:,j)~=0,j);
            if length(tempbeads)>1
                aggpos=systemB.pos(tempbeads,:);
                clear systemTempAgg
                systemTempAgg.attype=repmat('B',size(aggpos,1),1);
                systemTempAgg.pos=aggpos;
                systemTempAgg.dim=[1e20 1e20 1e20];
                systemTempAgg.mass=ones(size(systemTempAgg.attype));
                systemTempAgg.bonds=ones(0,2);
                systemTempAgg.angles=[];
                for k=1:length(tempbeads)-1
                    systemTempAgg.bonds(end+1,:)=[tempbeads(k) tempbeads(k+1)]-1;
                end
                if size(systemTempAgg.attype,1)>10
                    [singleAgglist]=calc_aggregates_beads(0,systemTempAgg,1.5,1);  %attype,bonds,dim,mass,pos,angles

                    aggposwrap=aggpos;
                    for i=2:singleAgglist.numaggs
                        for j=1:3
                            %try moving both directions and see
                            pos2=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),:);
                            pos1=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,1)~=0,1),:);
                            [distancesO,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2);
                            offset=[0 0 0];
                            offset(j)=totaldim(j);
                            [distancesL,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2-offset);
                            [distancesR,squareformdistances]=MinimumDistance([1e20 1e20 1e20],pos1,pos2+offset);
                            if min(distancesL)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)-totaldim(j);
                            elseif min(distancesR)<min(distancesO)
                                aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)+totaldim(j);
                            end
%                             if abs(singleAgglist.AggCOM(1,j)-singleAgglist.AggCOM(i,j))>totaldim(j)/2
%                                 if singleAgglist.AggCOM(1,j)>singleAgglist.AggCOM(i,j) 
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)+totaldim(j);
%                                 else
%                                     aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)=aggposwrap(singleAgglist.aggregates(singleAgglist.aggregates(:,i)~=0,i),j)-totaldim(j);
%                                 end
%                             end
                        end
                    end
                    % close all
                    aggposx=aggposwrap(:,1);
                    aggposy=aggposwrap(:,2);
                    aggposz=aggposwrap(:,3);
                    plot3(aggposx,aggposy,aggposz,'bs')
                    hold on
                    xlabel('x')
                    ylabel('y')
                    zlabel('z')

                    %change positions
                    systemB.pos(tempbeads,:)=aggposwrap;
                    
                    %record SA and vol
                    shp=alphaShape(aggposwrap(:,1),aggposwrap(:,2),aggposwrap(:,3));
                    plot(shp)
                    VOL=VOL+volume(shp)*size(aggposwrap,1);
                    SA=SA+surfaceArea(shp)*size(aggposwrap,1);
                    SAVOLrat=SAVOLrat+(surfaceArea(shp)/volume(shp))*size(aggposwrap,1);
                    Sphere=Sphere+(((pi^(1/3))*(6*volume(shp))^(2/3))/surfaceArea(shp))*size(aggposwrap,1);
                    totalbeads2=totalbeads2+size(aggposwrap,1);
                    

                    %bondtogether
                    [singleAgglist2]=calc_aggregates_beads(0,systemTempAgg,1.5,1);
                    for k=1:singleAgglist2.numaggs
                        tempbeads2=singleAgglist2.aggregates(singleAgglist2.aggregates(:,k)~=0,k);
                        for p=1:length(tempbeads2)-1
                            systemB.bonds(end+1,:)=[tempbeads(tempbeads2(p)) tempbeads(tempbeads2(p+1))]-1;
                        end
                    end
                end
            end
        end

    %     plot3(systemB.pos(:,1),systemB.pos(:,2),systemB.pos(:,3),'mo')
        %%%Chain Stats
        Bchainstats=Chain_Statistics_single(0,systemB,0)
        Bchainstats.Rg_eigenvalues=sort(Bchainstats.Rg_eigenvalues,2,'descend')

        %Calc Asphereicity
        clear E1 E2 asphere totalbeads
        E1=0;
        E2=0;
        totalbeads=0;
    %     asphere=0;
        nbeadsperchain=Bchainstats.nbeads_per_chain(Bchainstats.nbeads_per_chain>1);
        for k=1:size(Bchainstats.Rg,1)
            if nbeadsperchain(k)>10
                E1=sum(Bchainstats.Rg_eigenvalues(k,:))^2;
                E2=Bchainstats.Rg_eigenvalues(k,1)*Bchainstats.Rg_eigenvalues(k,2)+Bchainstats.Rg_eigenvalues(k,2)*Bchainstats.Rg_eigenvalues(k,3)+Bchainstats.Rg_eigenvalues(k,1)*Bchainstats.Rg_eigenvalues(k,3);
                asphere(k)=(1-3*(E2/E1))*nbeadsperchain(k);
                totalbeads=totalbeads+nbeadsperchain(k);
            else
                asphere(k)=NaN;
            end

        end

        %Calc eignvalues (really just remove the ones for very small domains)
        %and aspect ratios
        clear Rg_eig AspectRat
        for k=1:length(nbeadsperchain)
            if nbeadsperchain(k)>10
                Rg_eig(k,:)=Bchainstats.Rg_eigenvalues(k,:)*nbeadsperchain(k);
                AspectRat(k,:)=[Rg_eig(k,2)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,1) Rg_eig(k,3)/Rg_eig(k,2)]*nbeadsperchain(k);
            else
                Rg_eig(k,:)=[NaN NaN NaN];
                AspectRat(k,:)=[NaN NaN NaN];
            end
        end




        Results(counter).Name=folder;
        Results(counter).B.NumDomains=sum(nbeadsperchain>10);
        Results(counter).B.asphere=nansum(asphere)/totalbeads;
        Results(counter).B.Rg_eignevalues=nansum(Rg_eig,1)/totalbeads;
        Results(counter).B.AspectRatios=nansum(AspectRat,1)/totalbeads;
        Results(counter).B.VOL=VOL/totalbeads2;
        Results(counter).B.SA=SA/totalbeads2;
        Results(counter).B.SAVOLrat=SAVOLrat/totalbeads2;
        Results(counter).B.Sphere=Sphere/totalbeads2;
    end
    
    Results.B
    end
end
            