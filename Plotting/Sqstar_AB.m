clear all
close all

load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/maxSq_A.mat')
maxSq_A=maxSq;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/maxSq_B.mat')
maxSq_A1=maxSq;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/maxSq_B_A2.mat')
maxSq_A2=maxSq;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/maxSq_B_A3.mat')
maxSq_A3=maxSq;

figure
plot(1:12,maxSq_A1,'o-k','LineWidth',2,'MarkerFaceColor','k')
hold on
plot(1:12,maxSq_A2,'o-r','LineWidth',2,'MarkerFaceColor','r')
% plot(1:12,maxSq_A3,'o-b','LineWidth',2,'MarkerFaceColor','b')
xlabel('N_B ')
        ylabel('S(q*)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])


figure
plot(1:6,maxSq_A1,'o-k','LineWidth',2,'MarkerFaceColor','k')

xlabel('N_A')
        ylabel('S(q*)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
% title(['\fontsize{16}black {\color{magenta}magenta \color[rgb]{0 .5 .5}teal \color{red}red} black again'])