close all
clear all

load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/DiblocK_Shape_Results_1.mat')
Results1=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/ResultsRedo.mat')
ResultsRedo=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Diblock_Shape_results_2B.mat')
ResultsB=Results;


for i=1:size(ResultsRedo,2)
    for j=1:size(Results1,2)
        if strcmp(ResultsRedo(i).Name,Results1(j).Name)
            Results1(j).A=ResultsRedo(i).A;
            Results1(j).D=ResultsRedo(i).D;
        end
    end
end

for i=1:size(ResultsB,2)
    for j=1:size(Results1,2)
        if strcmp(ResultsB(i).Name,Results1(j).Name)
            Results1(j).B=ResultsB(i).B;
        end
    end
end
Results=Results1;

    figure
  % Plot the ternary axis system
  [h,hg,htick]=terplot;
  counter=0;

for i=1:size(Results,2)
    if ~isempty(Results(i).D)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        if numB>6
            counter=counter+1;

            fracA=numA/(numA+numB*2+numD);
            fracD=numD/(numA+numB*2+numD);
            fracB=1-fracA-fracD;

            c1(counter)=fracA;
            c2(counter)=fracB;
            c3(counter)=fracD;
%             d(counter)=(Results(i).D.AspectRatios(2))
            if Results(i).D.AspectRatios(1)>0.5 && Results(i).D.AspectRatios(2)<0.1
                d(counter)=0; %lam
            elseif Results(i).D.AspectRatios(1)>0.33 && Results(i).D.Sphere>0.5
                d(counter)=1; %sphere
            elseif Results(i).D.AspectRatios(1)>0.4
                d(counter)=2; %gyr
            else
                d(counter)=3; %cyl
            end
            if numD==1
                d(counter)=4; % mixed
            end

            %Do for Reflection
            counter=counter+1;
            c1(counter)=fracD;
            c2(counter)=fracB;
            c3(counter)=fracA;
%             d(counter)=(Results(i).D.AspectRatios(2))%-0.2012)/(0.6343-0.2012);
            d(counter)=d(counter-1);
        end
    end
end
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).A)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        if numB>6
            counter=counter+1;

            fracA=numA/(numA+numB*2+numD);
            fracD=numD/(numA+numB*2+numD);
            fracB=1-fracA-fracD;

            c1_A(counter)=fracA;
            c2_A(counter)=fracB;
            c3_A(counter)=fracD;
%             d_A(counter)=(Results(i).A.AspectRatios(2))%-0.2012)/(0.6343-0.2012);
            if Results(i).A.AspectRatios(1)>0.5 && Results(i).A.AspectRatios(2)<0.1
                d_A(counter)=0; %lam
            elseif Results(i).A.AspectRatios(1)>0.33 && Results(i).A.Sphere>0.5
                d_A(counter)=1; %sphere
            elseif Results(i).A.AspectRatios(1)>0.4
                d_A(counter)=2; %gyr
            else
                d_A(counter)=3; %cyl
            end
            if numA==1
                d_A(counter)=4;% mixed
            end
            %Repeat for Reflected
            counter=counter+1;
            c1_A(counter)=fracD;
            c2_A(counter)=fracB;
            c3_A(counter)=fracA;
%             d_A(counter)=(Results(i).A.AspectRatios(2))%-0.2012)/(0.6343-0.2012);
            d_A(counter)=d_A(counter-1);
        end
    end
end

if exist('d_A','var')==0
    mind=min(d);
    maxd=max(d);
    
else
    mind=min([min(d) min(d_A)]);
    maxd=max([max(d) max(d_A)]);  
    d_A=(d_A-mind)/(maxd-mind);
end


d=(d-mind)/(maxd-mind);

cmax=maxd;
cmin=mind;
% d=(log(d)-min(log(d)))/(max(log(d)-min(log(d))));

    
    clear colormap
    colormap copper
    CM=colormap;
    CM=AndyColorMap([2 3 4 7 1]);
%     if i==67
%         i==i
%     end
    
%     [hd,hcb]=ternaryc(fracA,fracB,fracD,round(d(i)*size(CM,1)),'o',20,CM);
    [hd,hcb]=ternaryc(c1,c3,c2,round(d*(size(CM,1)-1))+1,'o',9,CM);
if exist('d_A','var')==1
    hold on
    [hd,hcb]=ternaryc(c1_A,c3_A,c2_A,round(d_A*(size(CM,1)-1))+1,'o',4.5,CM);
end
  hlabels=terlabel('A','D','BC');
  
  f=gcf;
  f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23 11 6.1 5])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
  CB=colorbar('EastOutside')
  caxis([cmin cmax])
  CB.Position=[0.9300 0.2000 0.0150 0.6500];

