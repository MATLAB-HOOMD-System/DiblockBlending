close all
clear all

load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_12_13_19.mat')
% 
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/DiblocK_Shape_Results_1.mat')
% Results1=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/ResultsRedo.mat')
% ResultsRedo=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Diblock_Shape_results_2B.mat')
% ResultsB=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix1.mat')
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix2.mat')
% Results_fix2=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix3.mat')
% Results_fix3=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix4.mat')
% Results_fix4=Results;
% 
% for i=1:size(ResultsRedo,2)
%     for j=1:size(Results1,2)
%         if strcmp(ResultsRedo(i).Name,Results1(j).Name)
%             Results1(j).A=ResultsRedo(i).A;
%             Results1(j).D=ResultsRedo(i).D;
%         end
%     end
% end
% 
% for i=1:size(ResultsB,2)
%     for j=1:size(Results1,2)
%         if strcmp(ResultsB(i).Name,Results1(j).Name)
%             Results1(j).B=ResultsB(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix1,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix1(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix1(i).A;
%             Results1(j).D=Results_fix1(i).D;
%             Results1(j).B=Results_fix1(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix2,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix2(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix2(i).A;
% %             Results1(j).D=Results_fix2(i).D;
%             Results1(j).B=Results_fix2(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix3,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix3(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix3(i).A;
% %             Results1(j).D=Results_fix3(i).D;
%             Results1(j).B=Results_fix3(i).B;
%         end
%     end
% end
% 
% 
% for i=1:size(Results_fix4,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix4(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix4(i).A;
%             Results1(j).D=Results_fix4(i).D;
%             Results1(j).B=Results_fix4(i).B;
%         end
%     end
% end
% Results=Results1;


  counter=0;

  
  %% BC
for i=1:size(Results,2)
    if ~isempty(Results(i).B)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if numB>1 || numD>12
            counter=counter+1;

            

            cB1(counter)=fracA;
            cB2(counter)=fracB;
            cB3(counter)=fracD;
            d_BS(counter)=Results(i).B.Sphere
            d_BA1(counter)=Results(i).B.AspectRatios(1);
            d_BA2(counter)=Results(i).B.AspectRatios(2);


            %Do for Reflection
            counter=counter+1;
            cB1(counter)=fracD;
            cB2(counter)=fracB;
            cB3(counter)=fracA;
            d_BS(counter)=d_BS(counter-1);
            d_BA1(counter)=d_BA1(counter-1);
            d_BA2(counter)=d_BA2(counter-1);
        end
    end
end

%% A
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).A)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if (numA>1 || numB<=4) && (numB>1 || numD>12) && (numA>1 || numB<=6)
            counter=counter+1;

            cA1(counter)=fracA;
            cA2(counter)=fracB;
            cA3(counter)=fracD;
            d_AS(counter)=Results(i).A.Sphere
            d_AA1(counter)=Results(i).A.AspectRatios(1);
            d_AA2(counter)=Results(i).A.AspectRatios(2);


            %Do for Reflection
            counter=counter+1;
            cA1(counter)=fracD;
            cA2(counter)=fracB;
            cA3(counter)=fracA;
            d_AS(counter)=d_AS(counter-1);
            d_AA1(counter)=d_AA1(counter-1);
            d_AA2(counter)=d_AA2(counter-1);
        end
    end
end

%% D
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).D)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if (numB>1 || numD>12) && (numD>1 || numA>1)
            counter=counter+1;

            cD1(counter)=fracA;
            cD2(counter)=fracB;
            cD3(counter)=fracD;
            d_DS(counter)=Results(i).D.Sphere
            d_DA1(counter)=Results(i).D.AspectRatios(1);
            d_DA2(counter)=Results(i).D.AspectRatios(2);


            %Do for Reflection
            counter=counter+1;
            cD1(counter)=fracD;
            cD2(counter)=fracB;
            cD3(counter)=fracA;
            d_DS(counter)=d_DS(counter-1);
            d_DA1(counter)=d_DA1(counter-1);
            d_DA2(counter)=d_DA2(counter-1);
        end
    end
end



minS=min([min(d_AS) min(d_BS) min(d_DS)]);
minA1=min([min(d_AA1) min(d_BA1) min(d_DA1)]);
minA2=min([min(d_AA2) min(d_BA2) min(d_DA2)]);

maxS=max([max(d_AS) max(d_BS) max(d_DS)]);
maxA1=max([max(d_AA1) max(d_BA1) max(d_DA1)]);
maxA2=max([max(d_AA2) max(d_BA2) max(d_DA2)]);

d_AS=(d_AS-minS)/(maxS-minS);
d_BS=(d_BS-minS)/(maxS-minS);
d_DS=(d_DS-minS)/(maxS-minS);

d_AA1=(d_AA1-minA1)/(maxA1-minA1);
d_BA1=(d_BA1-minA1)/(maxA1-minA1);
d_DA1=(d_DA1-minA1)/(maxA1-minA1);

d_AA2=(d_AA2-minA2)/(maxA2-minA2);
d_BA2=(d_BA2-minA2)/(maxA2-minA2);
d_DA2=(d_DA2-minA2)/(maxA2-minA2);



% if exist('d_A','var')==0
%     mind=min(d);
%     maxd=max(d);
%     
% else
%     mind=min([min(d) min(d_A) min(d_D)]);
%     maxd=max([max(d) max(d_A) max(d_D)]);  
%     d_A=(d_A-mind)/(maxd-mind);
%     d_D=(d_D-mind)/(maxd-mind);
% end
% 
% 
% d=(d-mind)/(maxd-mind);
% 
% cmax=maxd;
% cmin=mind;
% d=(log(d)-min(log(d)))/(max(log(d)-min(log(d))));

    
    clear colormap
    colormap copper
    CM=colormap;
    CM1=cbrewer('seq','Blues',64,'PCHIP');
    CM2=cbrewer('seq','Reds',64,'PCHIP');
    CM3=cbrewer('seq','Purples',64,'PCHIP');
%     CM=AndyColorMap([2 3 4 7 1]);
%     if i==67
%         i==i
%     end
    

%% A plots
figure
% Plot the ternary axis system
[h,hg,htick]=terplot;
[hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA1*(size(CM1,1)-1))+1,'o',10,CM1);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AS*(size(CM1,1)-1))+1,'o',15/3,CM3);
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');

colorbar off
%pause
%export_fig 'A_AR1' -png -r800 -a1
%pause

figure
% Plot the ternary axis system
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA1*(size(CM1,1)-1))+1,'o',15,CM1);
[hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA2*(size(CM1,1)-1))+1,'o',10,CM2);
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');

colorbar off
%pause
%export_fig 'A_AR2' -png -r800 -a1
%pause

figure
% Plot the ternary axis system
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA1*(size(CM1,1)-1))+1,'o',15,CM1);
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
[hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AS*(size(CM1,1)-1))+1,'o',10,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');

colorbar off
%pause
%export_fig 'A_PHI' -png -r800 -a1
%pause

%% B Plots
figure
% Plot the ternary axis system
[h,hg,htick]=terplot;
[hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA1*(size(CM1,1)-1))+1,'o',10,CM1);
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'B_AR1' -png -r800 -a1
%pause

figure
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA1*(size(CM1,1)-1))+1,'o',15,CM1);
[hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA2*(size(CM1,1)-1))+1,'o',10,CM2);
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'B_AR2' -png -r800 -a1
%pause

figure
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA1*(size(CM1,1)-1))+1,'o',15,CM1);
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
[hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BS*(size(CM1,1)-1))+1,'o',10,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'B_PHI' -png -r800 -a1
%pause

%% D plots

figure
[h,hg,htick]=terplot;
[hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',10,CM1);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf, 'units', 'inches');
%         f.Position=[17.9789 15.3684 5.8947+1.5 4.4211]
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'D_AR1' -png -r800 -a1
%pause

figure
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',15,CM1);
[hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA2*(size(CM1,1)-1))+1,'o',10,CM2);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf, 'units', 'inches');
%         f.Position=[17.9789 15.3684 5.8947+1.5 4.4211]
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'D_AR2' -png -r800 -a1
%pause

figure
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',15,CM1);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
[hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf, 'units', 'inches');
%         f.Position=[17.9789 15.3684 5.8947+1.5 4.4211]
        set(gcf,'color','w');
        set(gca,'color','None');
colorbar off
%pause
%export_fig 'D_PHI' -png -r800 -a1
%pause

figure
[h,hg,htick]=terplot;
[hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',10,CM1);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA2*(size(CM1,1)-1))+1,'o',15*2/3,CM2);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',15/3,CM3);
for i=1:size(hd,2)
    hd(i).MarkerEdgeColor='k';
end
handels(1)=text(0.5,-0.08,'\phi_D','horizontalalignment','center');
handels(2)=text(0.15,sqrt(3)/4+0.05,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.85,sqrt(3)/4+0.05,'\phi_B_C','horizontalalignment','center','rotation',-60);
f=gcf;
        h1=gca;
        set(gcf, 'units', 'inches');
        f.Position=[17.9789 15.3684 5.8947+1.5 4.4211]
        set(gcf,'color','w');
        set(gca,'color','None');
% colorbar off
        
h1.Position=[0.0500 0.1100 0.1 0.8150];
colormap(h1,CM1)
CB1=colorbar(h1,'horiz')
caxis([minA1 maxA1])
CB1.Position=[0.4 0.2 0.45 0.03]
ylabel(CB1,'<AR_1>')
h2=axes
h2.Visible='off'
colormap(h2,CM2)
CB2=colorbar(h2,'horiz')
caxis([minA2 maxA2])
CB2.Position=[0.4 0.35 0.45 0.03]
ylabel(CB2,'<AR_2>')
h3=axes
h3.Visible='off'
colormap(h3,CM3)
CB3=colorbar(h3,'horiz')
caxis([minS maxS])
CB3.Position=[0.4 0.5 0.45 0.03]
ylabel(CB3,'<\Psi>')
%export_fig 'colorbars' -png -r800 -a1
% 
% %     [hd,hcb]=ternaryc(fracA,fracB,fracD,round(d(i)*size(CM,1)),'o',20,CM);
% [hd,hcb]=ternaryc(c1_D,c3_D,c2_D,round(d_D*(size(CM,1)-1))+1,'o',20,CM);
% hold on
% [hd,hcb]=ternaryc(c1,c3,c2,round(d*(size(CM,1)-1))+1,'o',13,CM);
% if exist('d_A','var')==1
%     hold on
%     [hd,hcb]=ternaryc(c1_A,c3_A,c2_A,round(d_A*(size(CM,1)-1))+1,'o',5,CM);
%     
% end
%   hlabels=terlabel('A','D','BC');
%   
%   f=gcf;
%   f.Position=[1925          85        1916        1912];
% 
% %   [hg,htick,hcb]=tersurf(c1,c2,c3,d)
% 
% % xlabel('q')
% %         ylabel('S(q)')
% %         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
% %         leg=legend(legtext)
%         h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [23 11 6.1 5])
% 
% %         legend boxoff
% %         set(gca,'FontSize',10)
%         set(gcf,'color','w');
%         set(gca,'color','None');
% %         box on
% %         set(leg,'FontSize',7);
% %         set(h1,'TickLength',[.02 .1])
% %         set(h1,'XMinorTick','on')
% %         set(gca,'XLim',[0 2])
%         
% %         %export_fig 'Rt_example' -png -r800 -a1
%   CB=colorbar('EastOutside')
%   caxis([cmin cmax])
%   CB.Position=[0.9300 0.2000 0.0150 0.6500];

