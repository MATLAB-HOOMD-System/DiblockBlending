Data=[2	16	2		1.56	1.59
2	15	3		1.07	3.51
2	14	4		0.98	5.88
2	13	5		0.97	8.79
3	14	3		2.1	2.28
3	13	4		1.7	3.64
3	12	5		1.92	4.67
3	11	6		1.63	8.15
3	10	7		1.53	16.8
4	12	4		2.65	2.56
4	11	5		2.48	3.85
5	10	5		3.17	4
5	9	6		3.55	5.46
2	12	6		0.9419	18.25
2	11	7		0.9458	52.33
2	10	8		0.9301	44.33
3	9	8		1.0167	30.5
4	8	8		1	13
5	8	7		2.6	6.5
6	8	6		1.7647	1.7647
6	7	7		2.4286	2
4	9	7		2.61	8.57
4	10	6		2.27	6.18
];

for i=1:size(Data,1)
    if Data(i,1)<Data(i,3)
        Data(end+1,:)=Data(i,[3 2 1 4 5]);
    end
end
figure
% Plot the ternary axis system
[h,hg,htick]=terplot;
% line([0.25 1],[0.4325 0.4325],'LineWidth',2,'Color','k')
% CM2=cbrewer('div','RdYlBu',200);
% CM3=cbrewer('div','RdYlBu',200);
CM=cbrewer('div','RdYlGn',200);


fracA=Data(:,1)./(sum(Data(:,1:3),2)+Data(:,2));
fracB=Data(:,2)./(sum(Data(:,1:3),2)+Data(:,2));
fracD=Data(:,3)./(sum(Data(:,1:3),2)+Data(:,2));
% A1=round((Data(:,4)/2)*101);
% A2=round(((Data(:,4)-2)/(6-2))*99)+101;
% Aneighbor=A1;
% Aneighbor(A1>101)=A2(A1>101);
% Aneighbor(Aneighbor>200)=200;
Aneighbor=round((Data(:,4)/6)*199)+1;
Aneighbor(Aneighbor>200)=200;
CM1=cbrewer('div','RdYlGn',132);
CM2=cbrewer('div','RdYlGn',266);
CM=[CM1(1:67,:); CM2(134:266,:)];

% Aneighbor=round((Data(:,4)/max(Data(:,4)))*201);

Dneighbor=round((Data(:,5)/6)*199)+1;
Dneighbor(Dneighbor>200)=200;

hold on
[hd,hcb]=ternaryc(fracA,fracD,fracB,Dneighbor,'o',20,CM);
for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';   
end
[hd,hcb]=ternaryc(fracA,fracD,fracB,Aneighbor,'o',10,CM);
for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';   
end
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
% hlabels=terlabel('A','D','BC');
f=gcf;
colormap(CM);
CB=colorbar;
CB.Ticks=[0:0.5:6]/6;
CB.TickLabels=0:0.5:6;

        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [22.2000 3.8421 6.0316 5.8000])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');




a=gca;
a.XLim=[0.2384 0.7502];
a.YLim=[0.4299 0.8731];
a.Position=[0.1169 0.1100 0.5968 0.8150];
CB.Position=[0.85 0.25 0.036 0.54];
% handels=ones(3,1);
handels(1)=text(0.5,0.38,'\phi_D','horizontalalignment','center');
handels(2)=text(0.29,0.69,'\phi_A','horizontalalignment','center','rotation',60);
handels(3)=text(0.70,0.69,'\phi_{BC}','horizontalalignment','center','rotation',-60);

line([0.25,1],[0.43,0.43],'Color','k','LineWidth',2.5)
text(0.36,0.41,'0.4')
text(0.46,0.41,'0.3')
text(0.56,0.41,'0.2')
text(0.66,0.41,'0.1')
text(0.76,0.41,'0.0')
text(0.95,0.62,'\eta_A or \eta_D','rotation',90)
% export_fig 'Rt_example' -png -r800 -a1








% CB.Ticks=[0:20:200]/200;
% CB.TickLabels=[([0:20:100]/101)*2 (([120:20:200]-101)/99)*4+2]
% CB.Ticks=[([0:0.5:2]/2)*101/200,((([3:6]-2)/(6-2))*99+101)/200]
% CB.TickLabels=[0:0.5:2,3:6];

% 
% CM=cbrewer('div','RdYlBu',10)
% figure
% hold on
% for i=1:size(CM,1)
%     plot([0 1],[0 i],'-','Color',CM(i,:),'LineWidth',3);
% end