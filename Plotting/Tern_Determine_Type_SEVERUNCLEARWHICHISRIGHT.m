function [out]=Tern_Determine_Type(c1,c2,c3)
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_12_13_19.mat')
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/ResultsREP1.mat')
Results(76)=ResultsREP1;



% 
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/DiblocK_Shape_Results_1.mat')
% Results1=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/ResultsRedo.mat')
% ResultsRedo=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Diblock_Shape_results_2B.mat')
% ResultsB=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix1.mat')
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix2.mat')
% Results_fix2=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix3.mat')
% Results_fix3=Results;
% load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix4.mat')
% Results_fix4=Results;
% 
% for i=1:size(ResultsRedo,2)
%     for j=1:size(Results1,2)
%         if strcmp(ResultsRedo(i).Name,Results1(j).Name)
%             Results1(j).A=ResultsRedo(i).A;
%             Results1(j).D=ResultsRedo(i).D;
%         end
%     end
% end
% 
% for i=1:size(ResultsB,2)
%     for j=1:size(Results1,2)
%         if strcmp(ResultsB(i).Name,Results1(j).Name)
%             Results1(j).B=ResultsB(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix1,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix1(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix1(i).A;
%             Results1(j).D=Results_fix1(i).D;
%             Results1(j).B=Results_fix1(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix2,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix2(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix2(i).A;
% %             Results1(j).D=Results_fix2(i).D;
%             Results1(j).B=Results_fix2(i).B;
%         end
%     end
% end
% 
% for i=1:size(Results_fix3,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix3(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix3(i).A;
% %             Results1(j).D=Results_fix3(i).D;
%             Results1(j).B=Results_fix3(i).B;
%         end
%     end
% end
% 
% 
% for i=1:size(Results_fix4,2)
%     for j=1:size(Results1,2)
%         if strcmp(Results_fix4(i).Name,Results1(j).Name)
%             Results1(j).A=Results_fix4(i).A;
%             Results1(j).D=Results_fix4(i).D;
%             Results1(j).B=Results_fix4(i).B;
%         end
%     end
% end
% Results=Results1;


  counter=0;

  
  %% BC
for i=1:size(Results,2)
    if ~isempty(Results(i).B)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if numB>1 
            counter=counter+1;

            

            cB1(counter)=fracA;
            cB2(counter)=fracB;
            cB3(counter)=fracD;
            d_BS(counter)=Results(i).B.Sphere
            d_BA1(counter)=Results(i).B.AspectRatios(1);
            d_BA2(counter)=Results(i).B.AspectRatios(2);
            d_BType(counter)=dettype(Results(i).B.AspectRatios(1),Results(i).B.AspectRatios(2),Results(i).B.Sphere,c1,c2,c3)
%             if Results(i).B.AspectRatios(1)<0.432 && Results(i).B.Sphere<0.523
%                 d_BType(counter)=1; % Cylinder
%             elseif Results(i).B.AspectRatios(1)>0.432 && Results(i).B.AspectRatios(2)>0.16 && Results(i).B.Sphere<0.4
%                 d_BType(counter)=2; %Gy
%             elseif Results(i).B.Sphere>0.5
%                 d_BType(counter)=3; %sphere
%             elseif Results(i).B.AspectRatios(2)<0.2
%                 d_BType(counter)=4; %lam
%             else
%                 d_BType(counter)=5; % something else
%             end


            %Do for Reflection
            counter=counter+1;
            cB1(counter)=fracD;
            cB2(counter)=fracB;
            cB3(counter)=fracA;
            d_BS(counter)=d_BS(counter-1);
            d_BA1(counter)=d_BA1(counter-1);
            d_BA2(counter)=d_BA2(counter-1);
            d_BType(counter)=d_BType(counter-1)
        end
    end
end

%% A 
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).A)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if (numA>1 || numB<=4) && (numB>1 || numD>12) && (numA>1 || numB<=6) && numA>2
            counter=counter+1;

            cA1(counter)=fracA;
            cA2(counter)=fracB;
            cA3(counter)=fracD;
            d_AS(counter)=Results(i).A.Sphere
            d_AA1(counter)=Results(i).A.AspectRatios(1);
            d_AA2(counter)=Results(i).A.AspectRatios(2);
            d_AType(counter)=dettype(Results(i).A.AspectRatios(1),Results(i).A.AspectRatios(2),Results(i).A.Sphere,c1,c2,c3)
%             if Results(i).A.AspectRatios(1)<0.432 && Results(i).A.Sphere<0.523
%                 d_AType(counter)=1; % Cylinder
%             elseif Results(i).A.AspectRatios(1)>0.432 && Results(i).A.AspectRatios(2)>0.16 && Results(i).A.Sphere<0.4
%                 d_AType(counter)=2; %Gy
%             elseif Results(i).A.Sphere>0.5
%                 d_AType(counter)=3; %sphere
%             elseif Results(i).A.AspectRatios(2)<0.2
%                 d_AType(counter)=4; %lam
%             else
%                 d_AType(counter)=5; % something else
%             end


            %Do for Reflection
            counter=counter+1;
            cA1(counter)=fracD;
            cA2(counter)=fracB;
            cA3(counter)=fracA;
            d_AS(counter)=d_AS(counter-1);
            d_AA1(counter)=d_AA1(counter-1);
            d_AA2(counter)=d_AA2(counter-1);
            d_AType(counter)=d_AType(counter-1)
        end
    end
end

%% D
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).D)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if (numB>1 || numD>12) && (numD>1 || numA>1)
            counter=counter+1;

            cD1(counter)=fracA;
            cD2(counter)=fracB;
            cD3(counter)=fracD;
            d_DS(counter)=Results(i).D.Sphere
            d_DA1(counter)=Results(i).D.AspectRatios(1);
            d_DA2(counter)=Results(i).D.AspectRatios(2);
            d_DType(counter)=dettype(Results(i).D.AspectRatios(1),Results(i).D.AspectRatios(2),Results(i).D.Sphere,c1,c2,c3)
%             if Results(i).D.AspectRatios(1)<0.432 && Results(i).D.Sphere<0.523
%                 d_DType(counter)=1; % Cylinder
%             elseif Results(i).D.AspectRatios(1)>0.432 && Results(i).D.AspectRatios(2)>0.16 && Results(i).D.Sphere<0.4
%                 d_DType(counter)=2; %Gy
%             elseif Results(i).D.Sphere>0.5
%                 d_DType(counter)=3; %sphere
%             elseif Results(i).D.AspectRatios(2)<0.2
%                 d_DType(counter)=4; %lam
%             else
%                 d_DType(counter)=5; % something else
%             end


            %Do for Reflection
            counter=counter+1;
            cD1(counter)=fracD;
            cD2(counter)=fracB;
            cD3(counter)=fracA;
            d_DS(counter)=d_DS(counter-1);
            d_DA1(counter)=d_DA1(counter-1);
            d_DA2(counter)=d_DA2(counter-1);
            d_DType(counter)=d_DType(counter-1)
        end
    end
end



minS=min([min(d_AS) min(d_BS) min(d_DS)]);
minA1=min([min(d_AA1) min(d_BA1) min(d_DA1)]);
minA2=min([min(d_AA2) min(d_BA2) min(d_DA2)]);

maxS=max([max(d_AS) max(d_BS) max(d_DS)]);
maxA1=max([max(d_AA1) max(d_BA1) max(d_DA1)]);
maxA2=max([max(d_AA2) max(d_BA2) max(d_DA2)]);

d_AS=(d_AS-minS)/(maxS-minS);
d_BS=(d_BS-minS)/(maxS-minS);
d_DS=(d_DS-minS)/(maxS-minS);

d_AA1=(d_AA1-minA1)/(maxA1-minA1);
d_BA1=(d_BA1-minA1)/(maxA1-minA1);
d_DA1=(d_DA1-minA1)/(maxA1-minA1);

d_AA2=(d_AA2-minA2)/(maxA2-minA2);
d_BA2=(d_BA2-minA2)/(maxA2-minA2);
d_DA2=(d_DA2-minA2)/(maxA2-minA2);



% if exist('d_A','var')==0
%     mind=min(d);
%     maxd=max(d);
%     
% else
%     mind=min([min(d) min(d_A) min(d_D)]);
%     maxd=max([max(d) max(d_A) max(d_D)]);  
%     d_A=(d_A-mind)/(maxd-mind);
%     d_D=(d_D-mind)/(maxd-mind);
% end
% 
% 
% d=(d-mind)/(maxd-mind);
% 
% cmax=maxd;
% cmin=mind;
% d=(log(d)-min(log(d)))/(max(log(d)-min(log(d))));

    
    clear colormap
    colormap copper
    CM=colormap;
    CM1=cbrewer('seq','Blues',64,'PCHIP');
    CM2=cbrewer('seq','Reds',64,'PCHIP');
    CM3=cbrewer('seq','Purples',64,'PCHIP');
    CM=AndyColorMap([2 3 4 7 1]); %
%     if i==67
%         i==i
%     end
    

%% D plots

f = figure('visible','off');
% Plot the ternary axis system
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',30,CM1);

%   CB.Position=[0.9300 0.2000 0.0150 0.6500];



hold on
[hd,hcb]=ternaryc(cD1,cD3,cD2,d_DType,'o',25,CM);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
hlabels=terlabel('A','D','BC');
f=gcf;
%   f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
%   CB=colorbar('EastOutside')
%   caxis([cmin cmax])
%   CB.Position=[0.9300 0.2000 0.0150 0.6500];
h1.Position=[0.0500 0.1100 0.72 0.8150];
colorbar off


%% A plots

f = figure('visible','off');
% Plot the ternary axis system
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',30,CM1);

%   CB.Position=[0.9300 0.2000 0.0150 0.6500];



hold on
[hd,hcb]=ternaryc(cA1,cA3,cA2,d_AType,'o',25,CM);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
hlabels=terlabel('A','D','BC');
f=gcf;
%   f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
%   CB=colorbar('EastOutside')
%   caxis([cmin cmax])
%   CB.Position=[0.9300 0.2000 0.0150 0.6500];
h1.Position=[0.0500 0.1100 0.72 0.8150];
colorbar off


%% B plots

f = figure('visible','off');
% Plot the ternary axis system
[h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',30,CM1);

%   CB.Position=[0.9300 0.2000 0.0150 0.6500];



hold on
[hd,hcb]=ternaryc(cB1,cB3,cB2,d_BType,'o',25,CM);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
hlabels=terlabel('A','D','BC');
f=gcf;
%   f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
%   CB=colorbar('EastOutside')
%   caxis([cmin cmax])
%   CB.Position=[0.9300 0.2000 0.0150 0.6500];
h1.Position=[0.0500 0.1100 0.72 0.8150];
colorbar off

%% Combined Plot

CM=AndyColorMap([2 3 4 7 1]); %
CM(4,:)=[1 1 1];
CM2=[1 0 1;0 1 1;1 1 0;1 1 1];
typetosymbol={'h','p','o','s'}; %cylinder, gyr, sphere, lam
typetosizefactor=[1.3,1.2,0.9,1.1];
f = figure('visible','off'); 
[h,hg,htick]=terplot;
hold on
[hd,hcb]=ternaryc(cD1,cD3,cD2,d_DType,'o',20*2/3,CM);
for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';   
    hd(i).Marker=typetosymbol{d_DType(i)};
    hd(i).MarkerSize=hd(i).MarkerSize*typetosizefactor(d_DType(i));
    if cD2(i)<0.5  %cD1(counter)=fracA;    cD2(counter)=fracB;     cD3(counter)=fracD;
        hd(i).MarkerFaceColor=CM2(d_DType(i),:);
    end
end
[hd,hcb]=ternaryc(cA1,cA3,cA2,d_AType,'^',12*2/3,CM*1);
for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';
    hd(i).Marker=typetosymbol{d_AType(i)};
    hd(i).MarkerSize=hd(i).MarkerSize*typetosizefactor(d_AType(i));
    if cA2(i)<0.5  %cD1(counter)=fracA;    cD2(counter)=fracB;     cD3(counter)=fracD;
        hd(i).MarkerFaceColor=CM2(d_AType(i),:);
    end
end
hend=hd;
[hd,hcb]=ternaryc(cB1,cB3,cB2,d_BType,'p',6*2/3,CM*1);
for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';
    hd(i).Marker=typetosymbol{d_BType(i)};
    hd(i).MarkerSize=hd(i).MarkerSize*typetosizefactor(d_BType(i));
    if cB2(i)<0.5  %cD1(counter)=fracA;    cD2(counter)=fracB;     cD3(counter)=fracD;
        hd(i).MarkerFaceColor=CM2(d_BType(i),:);
    end
end
% 
%Mixed
[hx,hcb]=ternaryc(1/(2+18*2),1/(2+18*2),(18*2)/(2+18*2),1,'x',18*2/3,[0 0 0]);
hx(1).LineWidth=2;
%Macrophse sep
[hd,hcb]=ternaryc([7,8,9,10 11 12]/21,[12,11,10,9,8,7]/21,[1 1 1 1 1 1]/21,[1 1 1 1 1 1],'d',18*2/3,[0 0 0]);

hlabels=terlabel('\phi_A','\phi_D','\phi_B_C');
f=gcf;
%   f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   7 7])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
%   CB=colorbar('EastOutside')
%   caxis([cmin cmax])
%   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% h1.Position=[0.0500 0.1100 0.72 0.8150];
colorbar off



blankax=axes();
hend2=plot(0.5,0.5,'kp','MarkerFaceColor','b','MarkerSize',12*2/3);
blankax.XLim=[0 0.1];
blankax.Visible='off';


leg=legend([hx,hd(1), hend(18),hend(10),hend2,hend(80),hend(1),hend(4),hend(27)],{'mixed','macro-phase-separated','spheres','cylinders','branched/gyroid','lam','concentric spheres','concentric cylinders','concentric branched/gyroid'})
leg.Position=[0.6374 0.7306 0.3001 0.2850];
% leg=legend([hx,hd,hend(18)],{'mixed','macro-phase-separated','spheres'})
% leg=legend([hend(18)],{'spheres'})
% 
% 
% 
% 
% %% A plots
% figure
% % Plot the ternary axis system
% [h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA1*(size(CM1,1)-1))+1,'o',30,CM1);
% 
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% 
% 
% 
% hold on
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AA2*(size(CM1,1)-1))+1,'o',20,CM2);
% [hd,hcb]=ternaryc(cA1,cA3,cA2,round(d_AS*(size(CM1,1)-1))+1,'o',10,CM3);
% hlabels=terlabel('A','D','BC');
% f=gcf;
% %   f.Position=[1925          85        1916        1912];
% 
% %   [hg,htick,hcb]=tersurf(c1,c2,c3,d)
% 
% % xlabel('q')
% %         ylabel('S(q)')
% %         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
% %         leg=legend(legtext)
%         h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])
% 
% %         legend boxoff
% %         set(gca,'FontSize',10)
%         set(gcf,'color','w');
%         set(gca,'color','None');
% %         box on
% %         set(leg,'FontSize',7);
% %         set(h1,'TickLength',[.02 .1])
% %         set(h1,'XMinorTick','on')
% %         set(gca,'XLim',[0 2])
%         
% %         export_fig 'Rt_example' -png -r800 -a1
% %   CB=colorbar('EastOutside')
% %   caxis([cmin cmax])
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% h1.Position=[0.0500 0.1100 0.72 0.8150];
% colormap(h1,CM1)
% CB1=colorbar(h1)
% caxis([minA1 maxA1])
% CB1.Position=[0.800 0.2000 0.0150 0.6500]
% ylabel(CB1,'AR_1')
% h2=axes
% h2.Visible='off'
% colormap(h2,CM2)
% CB2=colorbar(h2)
% caxis([minA2 maxA2])
% CB2.Position=[0.870 0.2000 0.0150 0.6500]
% ylabel(CB2,'AR_2')
% h3=axes
% h3.Visible='off'
% colormap(h3,CM3)
% CB3=colorbar(h3)
% caxis([minS maxS])
% CB3.Position=[0.9400 0.2000 0.0150 0.6500]
% ylabel(CB3,'S')
% 
% %% B Plots
% figure
% % Plot the ternary axis system
% [h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA1*(size(CM1,1)-1))+1,'o',30,CM1);
% 
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% 
% 
% 
% hold on
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BA2*(size(CM1,1)-1))+1,'o',20,CM2);
% [hd,hcb]=ternaryc(cB1,cB3,cB2,round(d_BS*(size(CM1,1)-1))+1,'o',10,CM3);
% hlabels=terlabel('A','D','BC');
% f=gcf;
% %   f.Position=[1925          85        1916        1912];
% 
% %   [hg,htick,hcb]=tersurf(c1,c2,c3,d)
% 
% % xlabel('q')
% %         ylabel('S(q)')
% %         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
% %         leg=legend(legtext)
%         h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])
% 
% %         legend boxoff
% %         set(gca,'FontSize',10)
%         set(gcf,'color','w');
%         set(gca,'color','None');
% %         box on
% %         set(leg,'FontSize',7);
% %         set(h1,'TickLength',[.02 .1])
% %         set(h1,'XMinorTick','on')
% %         set(gca,'XLim',[0 2])
%         
% %         export_fig 'Rt_example' -png -r800 -a1
% %   CB=colorbar('EastOutside')
% %   caxis([cmin cmax])
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% h1.Position=[0.0500 0.1100 0.72 0.8150];
% colormap(h1,CM1)
% CB1=colorbar(h1)
% caxis([minA1 maxA1])
% CB1.Position=[0.800 0.2000 0.0150 0.6500]
% ylabel(CB1,'AR_1')
% h2=axes
% h2.Visible='off'
% colormap(h2,CM2)
% CB2=colorbar(h2)
% caxis([minA2 maxA2])
% CB2.Position=[0.870 0.2000 0.0150 0.6500]
% ylabel(CB2,'AR_2')
% h3=axes
% h3.Visible='off'
% colormap(h3,CM3)
% CB3=colorbar(h3)
% caxis([minS maxS])
% CB3.Position=[0.9400 0.2000 0.0150 0.6500]
% ylabel(CB3,'S')
% 
% %% D plots
% 
% figure
% % Plot the ternary axis system
% [h,hg,htick]=terplot;
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA1*(size(CM1,1)-1))+1,'o',30,CM1);
% 
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% 
% 
% 
% hold on
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DA2*(size(CM1,1)-1))+1,'o',20,CM2);
% [hd,hcb]=ternaryc(cD1,cD3,cD2,round(d_DS*(size(CM1,1)-1))+1,'o',10,CM3);
% hlabels=terlabel('A','D','BC');
% f=gcf;
% %   f.Position=[1925          85        1916        1912];
% 
% %   [hg,htick,hcb]=tersurf(c1,c2,c3,d)
% 
% % xlabel('q')
% %         ylabel('S(q)')
% %         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
% %         leg=legend(legtext)
%         h1=gca;
%         set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   12.3263   11.2421])
% 
% %         legend boxoff
% %         set(gca,'FontSize',10)
%         set(gcf,'color','w');
%         set(gca,'color','None');
% %         box on
% %         set(leg,'FontSize',7);
% %         set(h1,'TickLength',[.02 .1])
% %         set(h1,'XMinorTick','on')
% %         set(gca,'XLim',[0 2])
%         
% %         export_fig 'Rt_example' -png -r800 -a1
% %   CB=colorbar('EastOutside')
% %   caxis([cmin cmax])
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];
% h1.Position=[0.0500 0.1100 0.72 0.8150];
% colormap(h1,CM1)
% CB1=colorbar(h1)
% caxis([minA1 maxA1])
% CB1.Position=[0.800 0.2000 0.0150 0.6500]
% ylabel(CB1,'AR_1')
% h2=axes
% h2.Visible='off'
% colormap(h2,CM2)
% CB2=colorbar(h2)
% caxis([minA2 maxA2])
% CB2.Position=[0.870 0.2000 0.0150 0.6500]
% ylabel(CB2,'AR_2')
% h3=axes
% h3.Visible='off'
% colormap(h3,CM3)
% CB3=colorbar(h3)
% caxis([minS maxS])
% CB3.Position=[0.9400 0.2000 0.0150 0.6500]
% ylabel(CB3,'S')
% 
% % 
% % %     [hd,hcb]=ternaryc(fracA,fracB,fracD,round(d(i)*size(CM,1)),'o',20,CM);
% % [hd,hcb]=ternaryc(c1_D,c3_D,c2_D,round(d_D*(size(CM,1)-1))+1,'o',20,CM);
% % hold on
% % [hd,hcb]=ternaryc(c1,c3,c2,round(d*(size(CM,1)-1))+1,'o',13,CM);
% % if exist('d_A','var')==1
% %     hold on
% %     [hd,hcb]=ternaryc(c1_A,c3_A,c2_A,round(d_A*(size(CM,1)-1))+1,'o',5,CM);
% %     
% % end
% %   hlabels=terlabel('A','D','BC');
% %   
% %   f=gcf;
% %   f.Position=[1925          85        1916        1912];
% % 
% % %   [hg,htick,hcb]=tersurf(c1,c2,c3,d)
% % 
% % % xlabel('q')
% % %         ylabel('S(q)')
% % %         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
% % %         leg=legend(legtext)
% %         h1=gca;
% %         set(gcf, 'units', 'inches', 'pos', [23 11 6.1 5])
% % 
% % %         legend boxoff
% % %         set(gca,'FontSize',10)
% %         set(gcf,'color','w');
% %         set(gca,'color','None');
% % %         box on
% % %         set(leg,'FontSize',7);
% % %         set(h1,'TickLength',[.02 .1])
% % %         set(h1,'XMinorTick','on')
% % %         set(gca,'XLim',[0 2])
% %         
% % %         export_fig 'Rt_example' -png -r800 -a1
% %   CB=colorbar('EastOutside')
% %   caxis([cmin cmax])
% %   CB.Position=[0.9300 0.2000 0.0150 0.6500];

% save('./GIT/DiblockBlending/Plotting/types2.mat','d_BType','d_AType','d_DType')
d_BType2=d_BType;
d_AType2=d_AType;
d_DType2=d_DType;
load('./GIT/DiblockBlending/Plotting/types2.mat')

difftype=sum(d_BType2~=d_BType)+sum(d_AType2~=d_AType)+sum(d_DType2~=d_DType)
%lamtype
indsB=find(d_BType==4);
indsA=find(d_AType==4);
indsD=find(d_DType==4);
difflam=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
indsB=find(d_BType2==4);
indsA=find(d_AType2==4);
indsD=find(d_DType2==4);
difflam2=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
%gy
indsB=find(d_BType==2);
indsA=find(d_AType==2);
indsD=find(d_DType==2);
diffgy=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
indsB=find(d_BType2==2);
indsA=find(d_AType2==2);
indsD=find(d_DType2==2);
diffgy2=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
%sphere
indsB=find(d_BType==3);
indsA=find(d_AType==3);
indsD=find(d_DType==3);
diffsphere=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
% diffsphere=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_DType2(indsD)~=d_DType(indsD))
indsB=find(d_BType2==3);
indsA=find(d_AType2==3);
indsD=find(d_DType2==3);
diffsphere2=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))
% diffsphere2=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_DType2(indsD)~=d_DType(indsD))
%cyl
indsB=find(d_BType==1);
indsA=find(d_AType==1);
indsD=find(d_DType==1);
diffcyl=sum(d_BType2(indsB)~=d_BType(indsB))+sum(d_AType2(indsA)~=d_AType(indsA))+sum(d_DType2(indsD)~=d_DType(indsD))

out=[diffsphere,diffsphere2]
end

function [type]=dettype(Ar1,Ar2,phi,c1,c2,c3)
%     if Ar1<0.432 && Ar2<0.432 && phi<0.523
%         type=1; %cylinder
%     elseif Ar2>=0.16 && phi<0.4
%         type=2; %branch/gy
%     elseif phi>0.5
%         type=3; %sphere
%     else
%         type=4; %lam
%     end
% if Ar1>0.42 && Ar2<0.2
%     type=4;
%     elseif phi>0.523
%     type=3;
% elseif Ar1>0.5 && Ar2>=0.2
%     type=2;
% 
% else
%     type=1;
if Ar1>0.45 && Ar2<0.1
% if Ar1>0.45 && Ar2<0.1
    type=4; %lam
% elseif Ar1>0.45 && Ar2>0.1 && phi<0.5
elseif Ar1>0.45 && Ar2>0.1 && phi<0.50
    type=2; %gy
% elseif phi<c1
elseif phi<c1
    type=1; %cyl

else
    type=3; %sphere
end
end

