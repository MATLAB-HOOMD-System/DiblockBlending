clear all
close all
Data=[1 1 8 1;2 1 7 2;1 2 7 2;3 1 6 2; 2 2 6 2;1 3 6 2];

index=6;
 
for b=5:-1:1
    for a=10-b-1:-1:1
        c=10-b-a;
%         index=index+1;
        Data(end+1,:)=[a c b 3];
    end
end


CM=AndyColorMap([2 3 4 7 1]); %
CM(1,:)=[1 1 1];
CM(3,:)=[1 1 1];
CM(2,:)=[1 1 1];
% CM2=[1 0 1;0 1 1;1 1 0;1 1 1];
typetosymbol={'o','h','s','s'}; %cylinder, gyr, sphere, lam
typetosizefactor=[1.3,1.2,0.9,1.1];
figure 

[h,hg,htick]=terplot;
hold on
a=gca;
% a.XLimMode='manual';
% a.YLimMode='manual';
p1=plot (1e6,1e6,'ok')
p2=plot (1e6,1e6,'hk')
p3=plot (1e6,1e6,'sk')

[hd,hcb]=ternaryc(Data(:,1)/10,Data(:,2)/10,Data(:,3)/10,Data(:,4),'o',20,CM);
a.XLim=[0 1];
a.YLim=[0 0.866];
colors=cbrewer('qual','Set2',4);

for i=1:length(hd)
    hd(i).MarkerEdgeColor='k';   
    hd(i).Marker=typetosymbol{Data(i,4)};
    hd(i).LineWidth=2;
    hd(i).MarkerFaceColor=colors(Data(i,4),:);
%     hd(i).MarkerSize=hd(i).MarkerSize*typetosizefactor(d_DType(i));
%     if cD2(i)<0.5  %cD1(counter)=fracA;    cD2(counter)=fracB;     cD3(counter)=fracD;
%         hd(i).MarkerFaceColor=CM2(d_DType(i),:);
%     end
end
% legend([p1,p2,p3],'Sphere/CsCl','Cylinder/square lattice','Lamellae')
leg=legend(hd([1 2 7]),'Sphere/CsCl','Cylinder/square lattice','Lamellae')
colorbar off
legend boxoff
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   6   5.5])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        
        leg.Position=[0.7 0.8 0.16 0.2];
%         export_fig 'Rt_example' -png -r800 -a1