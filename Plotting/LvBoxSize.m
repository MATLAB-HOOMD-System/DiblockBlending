LvBox=importdata('/home/andrew/Pictures/Datathief/Fig5_a');
close all
figure
hold on

counter=1;
numpoints=4;
for i=9:-1:1
    legtext{i}=['A_' num2str(10-i) 'B_{' num2str(1) '}C_{' num2str(1) '}D_{' num2str(20-(10-i)-1) '}'];
    plot(LvBox(counter:counter+numpoints-1,1),LvBox(counter:counter+numpoints-1,2),'o-','Color',AndyColorMap(10-i,'second'),'MarkerFaceColor',AndyColorMap(10-i,'second'),'LineWidth',2)
    counter=counter+numpoints;
end

xlabel('Box Size')
        ylabel('L')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         [leg, objh]=legend(legtext,'Right','NumColumns',2)
        [leg, objh]=columnlegend(2, legtext,'FontSize',6);
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [10 12 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',5);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        set(gca,'YLim',[5 70])
        leg.Position=[0.13 0.43 0.6 0.5];
        

objhl = findobj(objh, 'type', 'line'); % objects of legend of type patch
set(objhl, 'Markersize', 0.1); % set marker size as desired
set(objhl, 'LineWidth', 1); % set marker size as desired


%         export_fig 'Rt_example' -png -r800 -a1