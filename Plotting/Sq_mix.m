% S(q)_max=[12.825, 

% [r,gr,q,Sq]=SingleSq('/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/A1B18C18D1/negativeACchi_matchtri20N_Nd_A1B18C18D1_1_restart.gsd','Selections1','name A')
% close all
% figure
% hold on
cm=colormap(parula(6));
% cm=AndyColorMap(6)
for i=1:6
    path1='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
    folder=['A' num2str(i) 'B' num2str(20-2*i) 'C' num2str(20-2*i) 'D' num2str(i)];
    file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
    [r,gr,q(:,i),Sq(:,i)]=SingleSq(file,'Selections1','name A')
    legtext{i}=['A_' num2str(i) 'B_{' num2str(20-2*i) '}C_{' num2str(20-2*i) '}D_' num2str(i)];
    
end
close all
figure
hold on
for i=1:6
%     plot(q(:,i),Sq(:,i),'Color',cm(i,:),'LineWidth',2)
    plot(q(:,i),Sq(:,i),'Color',AndyColorMap(i),'LineWidth',2)
end

xlabel('q')
        ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1