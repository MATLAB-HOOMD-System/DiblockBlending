close all
clear all

load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/DiblocK_Shape_Results_1.mat')
Results1=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/ResultsRedo.mat')
ResultsRedo=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Diblock_Shape_results_2B.mat')
ResultsB=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix1.mat')
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix2.mat')
Results_fix2=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix3.mat')
Results_fix3=Results;
load('/mnt/Shared_Data/Diblock_Blending/analysis_for_IMAN/Results_fix4.mat')
Results_fix4=Results;

for i=1:size(ResultsRedo,2)
    for j=1:size(Results1,2)
        if strcmp(ResultsRedo(i).Name,Results1(j).Name)
            Results1(j).A=ResultsRedo(i).A;
            Results1(j).D=ResultsRedo(i).D;
        end
    end
end

for i=1:size(ResultsB,2)
    for j=1:size(Results1,2)
        if strcmp(ResultsB(i).Name,Results1(j).Name)
            Results1(j).B=ResultsB(i).B;
        end
    end
end

for i=1:size(Results_fix1,2)
    for j=1:size(Results1,2)
        if strcmp(Results_fix1(i).Name,Results1(j).Name)
            Results1(j).A=Results_fix1(i).A;
            Results1(j).D=Results_fix1(i).D;
            Results1(j).B=Results_fix1(i).B;
        end
    end
end

for i=1:size(Results_fix2,2)
    for j=1:size(Results1,2)
        if strcmp(Results_fix2(i).Name,Results1(j).Name)
            Results1(j).A=Results_fix2(i).A;
%             Results1(j).D=Results_fix2(i).D;
            Results1(j).B=Results_fix2(i).B;
        end
    end
end

for i=1:size(Results_fix3,2)
    for j=1:size(Results1,2)
        if strcmp(Results_fix3(i).Name,Results1(j).Name)
            Results1(j).A=Results_fix3(i).A;
%             Results1(j).D=Results_fix3(i).D;
            Results1(j).B=Results_fix3(i).B;
        end
    end
end


for i=1:size(Results_fix4,2)
    for j=1:size(Results1,2)
        if strcmp(Results_fix4(i).Name,Results1(j).Name)
            Results1(j).A=Results_fix4(i).A;
            Results1(j).D=Results_fix4(i).D;
            Results1(j).B=Results_fix4(i).B;
        end
    end
end
Results=Results1;

    figure
  % Plot the ternary axis system
  [h,hg,htick]=terplot;
  counter=0;

  
  %% BC
for i=1:size(Results,2)
    if ~isempty(Results(i).B)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if fracD<=0.5 && fracA<=0.5 && fracB<=0.5 && numB>1
            counter=counter+1;

            

            c1(counter)=fracA;
            c2(counter)=fracB;
            c3(counter)=fracD;
            d(counter)=(Results(i).B.AspectRatios(2))


            %Do for Reflection
            counter=counter+1;
            c1(counter)=fracD;
            c2(counter)=fracB;
            c3(counter)=fracA;
            d(counter)=d(counter-1);
        end
    end
end

%% A
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).A)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if fracD<=0.5 && fracA<=0.5 && fracB<=0.5 && numB>1
            counter=counter+1;

            c1_A(counter)=fracA;
            c2_A(counter)=fracB;
            c3_A(counter)=fracD;
            d_A(counter)=(Results(i).A.AspectRatios(2));
%             if numA==1
%                 d_A(counter)=4;
%             end

            %Repeat for Reflected
            counter=counter+1;
            c1_A(counter)=fracD;
            c2_A(counter)=fracB;
            c3_A(counter)=fracA;
            d_A(counter)=d_A(counter-1);
        end
    end
end

%% D
counter=0;
for i=1:size(Results,2)
    if ~isempty(Results(i).D)
        
        i
        name=Results(i).Name
        s=split(convertCharsToStrings(name),["A" "B" "C" "D"]);
        numA=str2num(s(2));
        numB=str2num(s(3));
        numD=str2num(s(5));
        fracA=numA/(numA+numB*2+numD);
        fracD=numD/(numA+numB*2+numD);
        fracB=1-fracA-fracD;
        
        if fracD<=0.5 && fracA<=0.5 && fracB<=0.5 && numB>1
            counter=counter+1;

            c1_D(counter)=fracA;
            c2_D(counter)=fracB;
            c3_D(counter)=fracD;
            d_D(counter)=(Results(i).D.AspectRatios(2));
%             if numA==1
%                 d_A(counter)=4;
%             end

            %Repeat for Reflected
            counter=counter+1;
            c1_D(counter)=fracD;
            c2_D(counter)=fracB;
            c3_D(counter)=fracA;
            d_D(counter)=d_D(counter-1);
        end
    end
end

if exist('d_A','var')==0
    mind=min(d);
    maxd=max(d);
    
else
    mind=min([min(d) min(d_A) min(d_D)]);
    maxd=max([max(d) max(d_A) max(d_D)]);  
    d_A=(d_A-mind)/(maxd-mind);
    d_D=(d_D-mind)/(maxd-mind);
end


d=(d-mind)/(maxd-mind);

cmax=maxd;
cmin=mind;
% d=(log(d)-min(log(d)))/(max(log(d)-min(log(d))));

    
    clear colormap
    colormap copper
    CM=colormap;
%     CM=AndyColorMap([2 3 4 7 1]);
%     if i==67
%         i==i
%     end
    
%     [hd,hcb]=ternaryc(fracA,fracB,fracD,round(d(i)*size(CM,1)),'o',20,CM);
[hd,hcb]=ternaryc(c1_D,c3_D,c2_D,round(d_D*(size(CM,1)-1))+1,'o',20,CM);
hold on
[hd,hcb]=ternaryc(c1,c3,c2,round(d*(size(CM,1)-1))+1,'o',13,CM);
if exist('d_A','var')==1
    hold on
    [hd,hcb]=ternaryc(c1_A,c3_A,c2_A,round(d_A*(size(CM,1)-1))+1,'o',5,CM);
    
end
  hlabels=terlabel('A','D','BC');
  
  f=gcf;
  f.Position=[1925          85        1916        1912];

%   [hg,htick,hcb]=tersurf(c1,c2,c3,d)

% xlabel('q')
%         ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [23 11 6.1 5])

%         legend boxoff
%         set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
%         box on
%         set(leg,'FontSize',7);
%         set(h1,'TickLength',[.02 .1])
%         set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1
  CB=colorbar('EastOutside')
  caxis([cmin cmax])
  CB.Position=[0.9300 0.2000 0.0150 0.6500];

