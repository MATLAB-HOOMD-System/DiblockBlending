% S(q)_max=[12.825, 
clear all
close all
% [r,gr,q,Sq]=SingleSq('/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/A1B18C18D1/negativeACchi_matchtri20N_Nd_A1B18C18D1_1_restart.gsd','Selections1','name A')
% close all
% figure
% hold on
cm=colormap(parula(6));
% cm=AndyColorMap(6)
for i=16:-1:5
    path1='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
        if i<=-1
            folder=['A' num2str(i) 'B' num2str(20-i-2) 'C' num2str(20-i-2) 'D' num2str(2)];
            file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
            [r,gr,q(:,i),Sq(:,i)]=SingleSq(file,'Selections1','name A')
        else
            folder=['A' num2str(3) 'B' num2str(20-i-3) 'C' num2str(20-i-3) 'D' num2str(i)];
            file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
            [r(:,i),gr(:,i),q(:,i),Sq(:,i)]=SingleSq(file,'Selections1','name A','Selections2','name A')
        end
        maxSq(i)=max(Sq(:,i));
    legtext{18-i}=['A_' num2str(3) 'B_{' num2str(20-i-3) '}C_{' num2str(20-i-3) '}D_{' num2str(i) '}'];
    
end
close all
figure
hold on
for i=17:-1:6
%     plot(q(:,i),Sq(:,i),'Color',cm(i,:),'LineWidth',2)
    plot(q(:,i),Sq(:,i),'Color',AndyColorMap(18-i),'LineWidth',2)
end

xlabel('q')
        ylabel('S(q)')
%         legtext={'A_1B_legtext(i1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(gca,'XLim',[0 2])
        
%         export_fig 'Rt_example' -png -r800 -a1


figure
plot(1:18,maxSq,'k-','LineWidth',2)
xlabel('N_A')
        ylabel('S(q*)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
        leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])



figure
hold on
for i=16:-1:5
    plot(r(:,i),gr(:,i),'LineWidth',2,'Color',AndyColorMap(17-i))
end
xlabel('r')
        ylabel('g(r)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
%         set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(gca,'XLim',[0 22])
        set(gca,'YLim',[0 8])