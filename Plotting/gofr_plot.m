close all
cm=colormap(cool(6));
for i=1:6
    path1='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
    folder=['A' num2str(i) 'B' num2str(20-2*i) 'C' num2str(20-2*i) 'D' num2str(i) '/'];
    file=[path1 folder 'gofr_A_A.dat'];
    temp=importdata(file);
    if i==1;
        gofr(:,1)=temp(:,1);
    end
    gofr(:,i+1)=temp(:,2);
    plot(gofr(:,1),gofr(:,i+1),'Color',cm(i,:))
    hold on
end
legend({'1','2','3','4','5','6'})

figure
cm=colormap(cool(6));
for i=1:6
    path1='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
    folder=['A' num2str(i) 'B' num2str(20-2*i) 'C' num2str(20-2*i) 'D' num2str(i) '/'];
    file=[path1 folder 'gofr_A_BC.dat'];
    temp=importdata(file);
    if i==1;
        gofr(:,1)=temp(:,1);
    end
    gofr(:,i+1)=temp(:,2);
    plot(gofr(:,1),gofr(:,i+1),'Color',cm(i,:))
    hold on
end
legend({'1','2','3','4','5','6'})


