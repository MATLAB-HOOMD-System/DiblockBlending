% close all
% clear all
% S(q)_max=[12.825, 

% [r,gr,q,Sq]=SingleSq('/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/A1B18C18D1/negativeACchi_matchtri20N_Nd_A1B18C18D1_1_restart.gsd','Selections1','name A')
% close all
% figure
% hold on
cm=colormap(parula(6));
% cm=AndyColorMap(6)
for i=[16:16]
    path1='/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/';
        if i<=18
            folder=['A' num2str(2) 'B' num2str(i) 'C' num2str(i) 'D' num2str(20-2-i)];
            file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
            [r,gr,q(:,i),Sq(:,i)]=SingleSq(file,'Selections1','name A')
            maxSq(i)=max(Sq(:,i));
            legtext{i}=['A' num2str(1) 'B' num2str(i) 'C' num2str(i) 'D' num2str(20-1-i)];
        elseif i==13
            folder=['A' num2str(1) 'B' num2str(i) 'C' num2str(i) 'D' num2str(20-1-i)];
            file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
            [r,gr,q(:,7),Sq(:,7)]=SingleSq(file,'Selections1','name A')
            legtext{7}=['A' num2str(1) 'B' num2str(i) 'C' num2str(i) 'D' num2str(20-1-i)];
        else
            folder=['A' num2str(3) 'B' num2str(20-i-3) 'C' num2str(20-i-3) 'D' num2str(i)];
            file=[path1 folder '/' 'negativeACchi_matchtri20N_Nd_' folder '_1_restart.gsd'];
            [r,gr,q(:,i),Sq(:,i)]=SingleSq(file,'Selections1','name D','Selections2','name D')
        end
    
    
end
% for i=1:7
%     if i==7
%         legtext2{i}=legtext{12};
%     else
%         legtext2{i}=legtext{i};
%     end
% end
% legtext=legtext2;
close all
figure
hold on
for i=1:16
%     plot(q(:,i),Sq(:,i),'Color',cm(i,:),'LineWidth',2)
    plot(q(:,i),Sq(:,i),'Color',AndyColorMap(i),'LineWidth',2)
end

xlabel('q')
        ylabel('S(q)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
        leg=legend(legtext,'Location','eastoutside')
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

        legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
        set(gca,'XLim',[0 2])
%         set(gca,'YLim',[-100 800])
%         export_fig 'Rt_example' -png -r800 -a1


figure
plot(1:16,maxSq,'k-o','LineWidth',2,'MarkerFaceColor','k')
xlabel('N_B')
        ylabel('S(q*)')
%         legtext={'A_1B_1_8C_1_8D_1','A_1B_1_8C_1_8D_1'}
%         leg=legend(legtext)
        h1=gca;
        set(gcf, 'units', 'inches', 'pos', [3 3 3.5 3])

%         legend boxoff
        set(gca,'FontSize',10)
        set(gcf,'color','w');
        set(gca,'color','None');
        box on
        set(leg,'FontSize',7);
        set(h1,'TickLength',[.02 .1])
        set(h1,'XMinorTick','on')
%         set(gca,'XLim',[0 2])
% set(gca,'YLim',[0 800])