clear all 
close all
List=[];
for i=2:6;
    List(end+1,:)=[1 i];
end
for i=1:5;
    List(end+1,:)=[2 i];
end
for i=1:7;
    List(end+1,:)=[3 i];
end
for i=1:7;
    List(end+1,:)=[4 i];
end
for i=1:6;
    List(end+1,:)=[5 i];
end
for i=[1 3:5];
    List(end+1,:)=[6 i];
end
for i=3:4;
    List(end+1,:)=[7 i];
end

List=[3,3;2,6;2,7;2,8;3,8;4,8;5,7;6,6;6,7];

%% Calculate nearest neighbors
for filecount=1:1%:length(List)
    clear path1 path2 path3 file system systemA systemD system 2Aagglist Dagglist mindist AaveNearNeighbors DaveNearNeighbors N V EDGES BINS

    path1=['/mnt/Shared_Data/Icopy/ND/SecondAttempt/SecondRound/'];
    if filecount==5
        path1=['/mnt/Shared_Data/Icopy/ND/FirstAttempt/FirstRun/'];
    end
    if filecount==7 || filecount==8
        path1=['/mnt/Shared_Data/Icopy/ND/Worm_Reruns/Worm_Reruns_Half/50000000TS/'];
    end
    path2=['A' num2str(List(filecount,1)) 'B' num2str(20-List(filecount,1)-List(filecount,2)) 'C' num2str(20-List(filecount,1)-List(filecount,2)) 'D' num2str(List(filecount,2))]
    path3=['/negativeACchi_matchtri20N_Nd_' path2 '_1_final.xml'];
    
    file=[path1 path2 path3]

    system=Extract_XML(file);
    % system=remove_beads(system,find(system.attype~='A'));
    systemA=strip_beads(0,system,'A')
    systemA.dim=[1e6 1e6 1e6];
%     systemA=system;
    % system.chainstats=Chain_Statistics_single(0,system)
    [aggregatelist]=calc_aggregates_beads(0,systemA,1.5,1)
    Aagglist=aggregatelist;
    scatter3(Aagglist.AggCOM(:,1),Aagglist.AggCOM(:,2),Aagglist.AggCOM(:,3),'ob')
    hold on

    % for i=1:Aagglist.numaggs
    %     close all
    %     scatter3(Aagglist.AggCOM(i,1),Aagglist.AggCOM(i,2),Aagglist.AggCOM(i,3),'ob','MarkerFaceColor','b')
    %     hold on
    %     scatter3(system.pos(Aagglist.aggregates(Aagglist.aggregates(:,i)~=0,i),1),system.pos(Aagglist.aggregates(Aagglist.aggregates(:,i)~=0,i),2),system.pos(Aagglist.aggregates(Aagglist.aggregates(:,i)~=0,i),3),'ks')
    %     f=gcf;
    %     a=gca;
    %     axis([-17 17 -17 17 -17 17])
    %     f.Position=[1925         442        1731        1430];
    % end



    system=Extract_XML(file);
    % system=remove_beads(system,find(system.attype~='A'));
    systemD=strip_beads(0,system,'D')
%      systemD.dim=[1e6 1e6 1e6];
%     systemD=system;
    % system.chainstats=Chain_Statistics_single(0,system)
    [aggregatelist]=calc_aggregates_beads(0,systemD,1.5,1)
    % [aggregatelist,chainstats]=calc_aggregates(0,system,1,1)
    Dagglist=aggregatelist;
    scatter3(Dagglist.AggCOM(:,1),Dagglist.AggCOM(:,2),Dagglist.AggCOM(:,3),'or')

    system2.attype=[repmat('A',Aagglist.numaggs,1);repmat('D',Dagglist.numaggs,1)];

    system2.pos=[Aagglist.AggCOM;Dagglist.AggCOM]
    system2.bonds=[];
    system2.angles=[]
    system2.dim=system.dim;

    Write_GSD(system2,'test.gsd')

    clear distances mindist distcut
% system.dim=[1e6 1e6 1e6];
    %%calc closest distnace between unlike types
    for i=1:Aagglist.numaggs
        for j=1:Dagglist.numaggs
            Apos=systemA.pos(Aagglist.aggregates(Aagglist.aggregates(:,i)~=0,i),:);
            Dpos=systemD.pos(Dagglist.aggregates(Dagglist.aggregates(:,j)~=0,j),:);
            [distances,squareformdistances]=MinimumDistance(system.dim,Apos,Dpos);
            mindist(i,j)=min(distances);
        end
    end
    mindist=sort(mindist(:));
    plot(mindist,'-ok')
    distances=mindist;
    close all
    histogram(distances,'BinWidth',0.1)
    [N,EDGES] = histcounts(distances,'BinWidth',0.1)
    for i=1:length(N)
        V(i)=(4*pi/3)*EDGES(i+1)^3-(4*pi/3)*EDGES(i)^3;
        BINS(i)=(EDGES(i+1)+EDGES(i))/2;
    end

    radialdensity=N./(V*Aagglist.numaggs);
    % plot(BINS,radialdensity)
%     hold on
%     plot(BINS,N./Aagglist.numaggs,'ko-');

    distcut=EDGES(find(N==0,1)+1);
% distcut=1.1
%     distcut=1.1;
    if distcut>10
        distcut=1;
        warning('cutoff too big, setting to 1')
    end
    path2
    distcut
    AaveNearNeighbors(filecount)=sum(distances<distcut)/Aagglist.numaggs
    DaveNearNeighbors(filecount)=sum(distances<distcut)/Dagglist.numaggs
end


figure
plot(BINS,N,'k-','LineWidth',2)
xlabel('Distance')
ylabel('Number of domain pairs')
h1=gca;
set(gcf, 'units', 'inches', 'pos', [23.0000    4.7579   3.5 3])

% legend boxoff
set(gca,'FontSize',10)
set(gcf,'color','w');
set(gca,'color','None');
box on
% set(leg,'FontSize',7);
set(h1,'TickLength',[.02 .1])
set(h1,'XMinorTick','on')
% set(gca,'XLim',[0 2])

% export_fig 'Rt_example' -png -r800 -a1





%% radial density calc
% pos=Aagglist.AggCOM;
% dim=system.dim;
% %xdistances
% distances=(pdist(pos(:,1)));
% distances(distances>dim(1)/2)=dim(1)-distances(distances>dim(1)/2);
% distances=distances.^2;
% 
% % disp('after x distances')
% % showmemorytotal
% 
% %ydistances
% tempdistances=(pdist(pos(:,2)));
% tempdistances(tempdistances>dim(2)/2)=dim(2)-tempdistances(tempdistances>dim(2)/2);
% distances=distances+tempdistances.^2;
% 
% % disp('after y distances')
% % showmemorytotal
% 
% %zdistances
% tempdistances=(pdist(pos(:,3)));
% tempdistances(tempdistances>dim(3)/2)=dim(3)-tempdistances(tempdistances>dim(3)/2);
% distances=distances+tempdistances.^2;
% 
% distances=distances.^(1/2);
% % distances=squareform(distances);
% histogram(distances)
% [N,EDGES] = histcounts(distances)
% for i=1:length(N)
%     V(i)=(4*pi/3)*EDGES(i+1)^3-(4*pi/3)*EDGES(i)^3;
%     BINS(i)=(EDGES(i+1)+EDGES(i))/2;
% end
% 
% radialdensity=N./V;
% plot(BINS,radialdensity)
