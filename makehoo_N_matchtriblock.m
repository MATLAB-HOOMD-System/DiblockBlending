clear all
TotalN=20;
writepy2Nd=1;
writepyNd=1;
%Nt=2Nd (split between both diblocks, that is, Btri=B=C
counter=1;
for Atri=1:TotalN
    for Btri=2:2:TotalN-Atri-1;
        Dtri=TotalN-Atri-Btri; 
            A=Atri;
            B=Btri/2;
            C=B;
            D=Dtri;
            triconfigs2Nd(counter,:)=[Atri Btri Dtri];
            configs2Nd(counter,:)=[A B C D];
            counter=counter+1;
            if writepy2Nd==1
                tempwrite(A,B,C,D,'2Nd',1)
            end
    end
end
triconfigs2Nd/TotalN
configs2Nd/TotalN

%Nt=Nd (both blocks are the length of the triblock midblock)

counter=1;
for Atri=1:TotalN
    for Btri=1:1:TotalN-Atri-1;
        Dtri=TotalN-Atri-Btri;
        A=Atri;
            B=Btri;
            C=B;
            D=Dtri;
            triconfigsNd(counter,:)=[Atri Btri Dtri];
            configsNd(counter,:)=[A B C D];
            counter=counter+1;
            if writepyNd==1
                tempwrite(A,B,C,D,'Nd',1)
            end
    end
end
% TotalN_Nd=sum(configsNd,2);
% triconfigsNd/TotalN
% configsNd./repmat(TotalN_Nd,[1 4])

function tempwrite(A,B,C,D,type,runnum)
    if exist('runnum','var')~=1
        runnum=1;
    end
    addpath('/mnt/Shared_Data/Diblock_Blending/match_triblock/20beadchains/')
    filename=['negativeACchi_matchtri20N_' type '_A' num2str(A) 'B' num2str(B) 'C' num2str(C) 'D' num2str(D) '_' num2str(runnum)]
    fid1=fopen('/mnt/Shared_Data/Diblock_Blending/match_triblock/20beadchains/template.py','r');
    fid2=fopen([filename '.py'],'w','l');
    
    for i=1:13
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end
    
    x=fgetl(fid1);
    fprintf(fid2,['An=' num2str(A) '\n']);
    x=fgetl(fid1);
    fprintf(fid2,['Bn=' num2str(B) '\n']);
    x=fgetl(fid1);
    fprintf(fid2,['Cn=' num2str(C) '\n']);
    x=fgetl(fid1);
    fprintf(fid2,['Dn=' num2str(D) '\n']);
      
    for i=18:21
        x=fgetl(fid1);
        fprintf(fid2,x);
        fprintf(fid2,'\n');
    end
    
    x=fgetl(fid1);
    fprintf(fid2,['prefix=' '''negativeACchi_matchtri20N_' type '_A'' + str(An) +''B'' + str(Bn) + ''C'' + str(Cn) + ''D'' + str(Dn) + ''_' num2str(runnum) '''' '\n']);
    
    for i=23:91
        x=fgetl(fid1);
        fprintf(fid2,x);
        fprintf(fid2,'\n');
    end
end