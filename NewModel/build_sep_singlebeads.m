function [system] = build_sep_singlebeads(beadnames,numbeads,masses,dim,gsdname)
%Build single beads in box for various types.
%beadnames needs to be a list of bead names (single letter)
%numbeads needs to be a list of numbers of beads (vector) same size as
%beadnames
%masses must be the same length as numbeads and indicates masses of each type
%dim is a vector of dimensions. Beads will be placed evenly weighed by
%number along the x dimension
%it will be saved as a gsd if gsdname is defined

system.dim=dim;
numtypes=length(numbeads);
deltax=(numbeads/sum(numbeads))*dim(1);
system.natoms=sum(numbeads);
firstbeadflag=1;
for i=1:numtypes
    for j=1:numbeads(i)
        if firstbeadflag==1
            system.attype(1)=beadnames(1);
            system.mass(1)=masses(1);
            system.pos(1,:)=[rand()*deltax(1) rand(1,2).*dim(2:3)];
%             system.bonds=[];
            firstbeadflag=0;
        else
            system.attype(end+1)=beadnames(i);
            system.mass(end+1)=masses(i);
            if i==1
                system.pos(end+1,:)=[rand()*deltax(1) rand(1,2).*dim(2:3)];
            else
                system.pos(end+1,:)=[(sum(deltax(1:i-1))+rand()*deltax(i)) rand(1,2).*dim(2:3)];
            end
        end
    end
end
system.attype=system.attype';
system.mass=system.mass';
system.pos=system.pos-repmat(dim/2,size(system.pos,1),1);
if exist('gsdname','var')
    Write_GSD(system,gsdname);
end
        